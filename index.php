<!DOCTYPE html>
<html>
<head>
  <title>Imunisasi MR</title>
  <link rel="stylesheet" href="web-1/assets/styles/bootstrap.min.css" />
</head>
<body>
  <div class="container" style="margin-top: 30px;">
      <div class="row">
        <div class="col-md-6 offset-md-3">
          <div class="card">
            <img class="card-img-top" src="webfix2.png" alt="Web Versi Fix Revisi">
            <div class="card-body">
              <h5 class="card-title">Web Fix revisi</h5>
              <a href="web-fix-2" class="btn btn-primary">Lihat</a>
            </div>
          </div>
      </div>
    </div>
    <br>
    <div class="row justify-content-center">
      <div class="col-sm-5" style="padding-left:10px;padding-right:10px;">
          <div class="card" style="margin-top: 20px;">
            <img class="card-img-top" src="web1.jpg" alt="Web Versi 1">
            <div class="card-body">
              <h5 class="card-title">Web Versi 1</h5>
              <a href="web-1" class="btn btn-primary">Lihat</a>
            </div>
          </div>
      </div>
      <div class="col-sm-5" style="padding-left:10px;padding-right:10px;">
          <div class="card" style="margin-top: 20px;">
            <img class="card-img-top" src="web2.jpg" alt="Web Versi 2">
            <div class="card-body">
              <h5 class="card-title">Web Versi 2</h5>
              <a href="web-2" class="btn btn-primary">Lihat</a>
            </div>
          </div>
      </div>
      <div class="col-sm-5" style="padding-left:10px;padding-right:10px;">
          <div class="card" style="margin-top: 20px;">
            <img class="card-img-top" src="web3.jpg" alt="Web Versi 3">
            <div class="card-body">
              <h5 class="card-title">Web Versi 3</h5>
              <a href="web-3" class="btn btn-primary">Lihat</a>
            </div>
          </div>
      </div>
      <div class="col-md-5">
          <div class="card" style="margin-top: 20px;">
            <img class="card-img-top" src="webfix.png" alt="Web Versi Fix">
            <div class="card-body">
              <h5 class="card-title">Web Fix</h5>
              <a href="web-fix" class="btn btn-primary">Lihat</a>
            </div>
          </div>
      </div>
    </div>
  </div>

</body>
</html>