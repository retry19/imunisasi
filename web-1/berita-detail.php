<?php define('imunisasi', 'imunisasimr2020');
  include 'ref-berita.php';
  include 'header.php';  

  $index = find($_GET['s'],$news);
  $berita = $news[$index];

  $title = $berita['title'];

?>
    <!-- start content -->
    <div class="detail-berita my-4 container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="wrapper">
            <img src="<?php echo $berita['image']; ?>" alt="<?php echo $berita['title']; ?>" />
          </div>
          <small>(Sumber: <a href="<?php echo getHostUrl($berita['link_source']); ?>"><?php echo getHostUrl($berita['link_source']); ?></a>)</small>
          <small
            ><?php echo $berita['date']; ?><br /><?php echo $berita['author']; ?></small
          >
          <h4><?php echo $berita['title']; ?></h4>
          <label><?php echo $berita['reference']; ?></label>
          <?php echo $berita['content']; ?>
        </div>
      </div>
    </div>
    <!-- end content -->
<?php include 'footer.php'; ?>
