<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Berita :: Ayo Imunisasi MR';
  include 'ref-berita.php';
  include 'header.php';

?>
    <!-- start content -->
    <div class="berita">
      <div class="row justify-content-center">
        <?php 
          foreach ($news as $i => $row) 
          {
            if (($i+2)%2==0) 
            {
              echo '</div><div class="row justify-content-center">';
            }
            echo '<div class="col-md-5 mb-4">
                    <a href="berita-detail.php?s='.$row['slug'].'">
                      <div class="wrapper mb-3">
                        <img src="'.$row['image'].'" alt="'.$row['title'].'" />
                      </div>
                      <p>'.$row['title'].'</p>
                      <label class="mt-1">'.$row['reference'].'</label>
                    </a>
                  </div>';
          }
         ?> 
      </div>
    </div>
    <!-- end content -->
<?php include 'footer.php'; ?>