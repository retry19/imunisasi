<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  include 'header.php';

?>

    <!-- start banner -->
    <div class="row banner text-white">
      <div class="col-lg-7 banner-text">
        <h1>Ayo Imunisasi MR karena kesehatan anak merupakan investasi.</h1>
      </div>
      <div class="col-lg-5 banner-image">
        <img
          src="assets/images/main-photo.png"
          alt="Foto anak-anak sedang bermain"
          class="img-fluid"
        />
        <small>(sumber: dokumentasi pribadi)</small>
      </div>
    </div>
    <!-- end banner -->
    <!-- start berita -->
    <div class="berita-main container">
      <div class="row mb-3 berita-main-title">
        <div class="col">
          <h2>Berita</h2>
        </div>
      </div>
      <div class="row justify-content-between">
        <?php for ($i=0; $i < 3; $i++) 
        { 
          $berita = $news[$i];
          echo '<div class="col-md-3">
                  <a href="berita-detail.php?s='.$berita['slug'].'">
                    <div class="berita-main-wrapper">
                      <img src="'.$berita['image'].'" class="img-fluid mb-3" alt="'.$berita['title'].'"/>
                    </div>
                    <p>'.$berita['title'].'</p>
                  </a>
                </div>';
        } ?>
      </div>
    </div>
    <!-- end berita -->

<?php include 'footer.php'; ?>
  