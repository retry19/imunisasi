<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Peraturan UU :: Ayo Imunisasi MR';
  include 'header.php';

?>
    <!-- start banner -->
    <div class="row banner-content text-white">
      <img
        src="assets/images/peraturan-uu.jpg"
        alt="Ilustrasi landasan hukum"
        class="img-fluid"
      />
    </div>
    <!-- end banner -->
    <!-- start content -->
    <div class="container">
      <div class="row content">
        <div class="col-md-6 content-title">
          <small>Foto: Ilustrasi landasan hukum</small><br />
          <small style="font-size: 0.6rem;"
            >(Sumber:
            <a
              href="https://www.acq-intl.com"
              >https://www.acq-intl.com</a
            >
            )</small
          >
          <h2 class="mt-4">
            Tahukah Anda landasan hukum pelaksanaan imunisasi?
          </h2>
        </div>
        <div class="col-md-6 content-desc">
          <p>
            Berikut ini merupakan landasan hukum imunisasi.
          </p>
          <p>
            <img
              src="assets/images/landasan-hukum.jpg"
              alt="Landasan hukum imunisasi"
              class="img-fluid"
            />
          </p>
        </div>
      </div>
    </div>
    <!-- end content -->
<?php include 'footer.php'; ?>