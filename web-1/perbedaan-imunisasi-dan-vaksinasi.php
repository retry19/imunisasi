<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Perbedaan Imunisasi dan Vaksinasi :: Ayo Imunisasi MR';
  include 'header.php';

?>
    <!-- start banner -->
    <div class="row banner-content text-white">
      <img
        src="assets/images/perbedaan-imunisasi-dan-vaksin.jpg"
        alt="Ilustrasi perbedaan imunisasi dan vaksinasi"
        class="img-fluid"
      />
    </div>
    <!-- end banner -->
    <!-- start content -->
    <div class="container">
      <div class="row content">
        <div class="col-md-6 content-title">
          <small>Foto: Ilustrasi Perbedaan Imunisasi dan Vaksinasi</small><br />
          <small style="font-size: 0.6rem;"
            >(Sumber:
            <a
              href="https://www.thejakartapost.com"
              >https://www.thejakartapost.com</a
            >
            )</small
          >
          <h2 class="mt-4">Tahukah Anda bedanya imunisasi dan vaksinasi?</h2>
        </div>
        <div class="col-md-6 content-desc">
          <p>
            Berdasarkan Pusat Pengendalian dan Pencegahan Penyakit Amerika
            Serikat atau CDC, vaksinasi adalah suatu kegiatan memasukkan vaksin
            kedalam tubuh seseorang dengan tujuan untuk menghasilkan kekebalan
            terhadap suatu penyakit tertentu. Sedangkan imunisasi adalah suatu
            proses ketika seseorang menjadi kebal terhadap penyakit tertentu yg
            dilakukan melalui vaksinasi.
          </p>
          <p>
            Proses imunisasi dapat dianalogikan sebagai berikut, apabila
            seseorang terkena penyakit maka sistem kekebalan tubuh akan berjuang
            untuk melawan penyakit tersebut. Serupa dengan proses imunisasi,
            setelah dilakukan vaksinasi maka vaksin akan merangsang sistem
            kekebalan tubuh agar kebal dalam menghadapi penyakit tertentu
            dikemudian hari.
          </p>
        </div>
      </div>
    </div>
    <!-- end content -->
<?php include 'footer.php'; ?>