<?php defined('imunisasi') OR exit('No direct script access allowed');
function find($needle, $haystack) {
    foreach($haystack as $key=>$value){
       if(is_array($value) && array_search($needle, $value) !== false) {
          return $key;
       }
    }
    return false;
}
function getHostUrl($url)
{
	$p = parse_url($url);
	return isset($p['host'])?$p['scheme']."://".$p['host']:$url;
}

$news = [
	[
		'slug'=>'belum-capai-target-imunisasi-penularan-campak-rubella-masih-berisiko',
		'title'=>'Belum Capai Target Imunisasi, Penularan Campak-Rubella Masih Berisiko',
		'reference'=>'Detik.com',
		'image'=>'assets/images/berita-1.jpg',
		'link_source'=>'https://health.detik.com/berita-detikhealth/d-4374022/belum-capai-target-imunisasi-penularan-campak-rubella-masih-berisiko',
		'date'=>'Senin, 07 Jan 2019 14.23 WIB',
		'author'=>'Khadijah Nur Azizah - detikHealth',
		'content'=>'<p>
            Jakarta - Kampanye imunisasi campak-rubella atau MR yang berakhir
            para 31 Desember 2018 lalu secara keseluruhan mencapai 87,33 persen
            dari target 95 persen.
          </p>
          <p>
            "Cakupan yang sudah kita capai bersama belum cukup untuk melindungi
            seluruh masyarakat dari bahaya penyakit campak, rubella dan cacat
            lahir akibat rubella," ujar Direktur Jenderal Pencegahan dan
            Pengendalian Penyakit Kementerian Kesehatan, Anung Sugihantono, saat
            dijumpai di kantor Kementerian Kesehatan, Jl Rasuna Said Jakarta
            Selatan pada Senin (7/1/2019).
          </p>
          <p>
            Anung, sapaannya, menambahkan dibutuhkan cakupan imunisasi minimal
            95 persen agar terbentuk kekebalan masyarakat yang cukup untuk
            menurunkan kasus campak, rubella dan cacat lahir akibat rubella
            untuk mencegah Kejadian Luar Biasa.
          </p>
          <p>
            "Dari hampir semua provinsi, dalam kurun waktu 3 tahun terakhir,
            hanya ada 1 provinsi aman, 9 provinsi dengan risiko sedang dan 24
            provinsi masuk risiko tinggi," tambahnya.
          </p>
          <p>
            Sehubungan dengan hal tersebut, risiko terjadinya penyebaran
            penyakit Campak dan Rubella di masyarakat yang dapat mengakibatkan
            munculnya cacat lahir akibat rubella dan Kejadian Luar Biasa masih
            terus diwaspadai.
          </p>
          <p>
            "Secara keseluruhan, Kemenkes melakukan pemetaan risiko wilayah atau
            potensi wilayah yang harus diwaspadai. Pengamatan terus menerus
            terhadap PD3I (Penyakit-penyakit yang Dapat Dicegah dengan
            Imunisasi) harus dilanjutkan," tutupnya.
          </p>'
	],
	[
		'slug'=>'bukan-hanya-anak-anak-ini-pentingnya-wanita-juga-harus-imunisasi-mr',
		'title'=>'Bukan Hanya Anak-anak, Ini Pentingnya Wanita juga Harus Imunisasi MR',
		'reference'=>'Detik.com',
		'image'=>'assets/images/berita-2.jpg',
		'link_source'=>'https://health.detik.com/berita-detikhealth/d-4283309/bukan-hanya-anak-anak-ini-pentingnya-wanita-juga-harus-imunisasi-mr',
		'date'=>'Kamis, 01 Nov 2018 15.06 WIB',
		'author'=>'Widiya Wiyanti - detikHealth',
		'content'=>'<p>
	            Jakarta - Kampanye imunisasi Measles dan Rubella (MR) yang
	            dilangsungkan pemerintah saat ini masih berfokus pada anak-anak usia
	            9 bulan sampai 15 tahun. Namun ternyata vaksin ini juga sangat
	            penting bagi wanita, terutama wanita sebelum mengalami kehamilan.</p>
	          <p>Menurut dr Hindra Irawan Satari, SpA(K) dari Indonesian Technical
	            Advisory Group on Immunization (ITAGI) mengatakan bahwa vaksin MR
	            pada wanita sebelum hamil karena dapat mencegah tertularnya janin
	            terkena penyakit campak dan Rubella.</p>
	          <p>"Bayinya bisa cacat. Kalau dia ada antibodi terhadap rubella,
	            artinya dia rubella," ujarnya saat ditemui di Gedung Adhyatma
	            Kementerian Kesehatan RI, Jakarta Selatan, Kamis (1/11/2018).</p>
	          <p>Ibu hamil memiliki daya tahan tubuh yang cukup rendah sehingga
	            rentan sekali tertular. Mengingat penularan virus Rubella sangat
	            mudah hanya dengan percikan air liur.</p>
	          <p>"Dari percikan ludah. Sangat menular," tegas dr Hindra.</p>
	          <p>"Sebetulnya secara umum ke orang yang daya tahan tubuh tinggi nggak
	            berat, paling demam, ruam, pembesaran kelenjar getah bening. Sembuh
	            seminggu. Kalau dia menyerang ibu hamil, ibu hamil itu relatif daya
	            tahan tubuhnya rendah. kalau dia kena ibu hamil dan ibu hamil
	            menularkan ke bayinya, bayinya bisa cacat," jelasnya.</p>
	          <p>Maka dari itu untuk mencegah penularan virus campak dan Rubella,
	            setiap wanita sebelum hamil sebaiknya diimunisasi MR. dr Hindra
	            menyebut setidaknya dua kali dalam rentang waktu enam bulan.</p>
	          <p>Vaksin Rubella sendiri biasanya sudah termasuk dalam vaksin TORCH
	            (Toxoplasmosis, Rubella, Cytomegalovirus dan Herpes), namun masih
	            banyak wanita yang tidak mendapatkan vaksin ini sebelum menikah
	            karena kurangnya kesadaran.
	          </p>'
	],
	[
		'slug'=>'ini-dampaknya-jika-imunisasi-mr-tak-mencapai-target',
		'title'=>'Ini Dampaknya Jika Imunisasi MR Tak Mencapai Target',
		'reference'=>'Detik.com',
		'image'=>'assets/images/berita-3.jpg',
		'link_source'=>'https://health.detik.com/berita-detikhealth/d-4267741/ini-dampaknya-jika-imunisasi-mr-tak-mencapai-target',
		'date'=>'Senin, 22 Okt 2018 15:30 WIB',
		'author'=>'Widiya Wiyanti - detikHealth',
		'content'=>'<p>Jakarta - Per tanggal 22 Oktober 2018, capaian imunisasi Measles dan
            Rubella (MR) masih 63,36 persen dari taget 95 persen. Ada beberapa
            tantangan di setiap daerah di luar Pulau Jawa, salah satunya
            penolakan masyrakat.</p>
            <p>Penolakan masyarakat disebut oleh Direktur Pencegahan dan
            Pengendalian Penyakit Kementerian Kesehatan, Anung Sugihantono
            karena adanya keraguan akan kehalalan vaksin MR tersebut.</p>
          <p>Namun, Anung mengatakan bahwa tidak dilakukannya imunisasi MR ini
            bisa berdampak pada mewabahnya penyakit campak yang dapat menular
            dengan mudah, terutama pada ibu hamil. Risikonya pun akan sama
            seperti daerah yang sama sekali tidak melakukan imunisasi MR.</p>
          <p>"Kita ini setiap tahun mempunya 5 juta ibu yang sedang hamil, kalau
            dibagi per trimester, ini kan penularannya lebih banyak di trimester
            satu, artinya ada sepertiga dari 1,5 juta ibu hamil yang sedang di
            triwulan satu," jelas Anung saat ditemui di Gedung Adhyatma
            Kementerian Kesehatan RI, Jakarta Selatan, Senin (22/10/2018).</p>
            <p>"Taruh di luar Jawa 40 persen, 60 persen di Jawa. Berarti kan 40
            persen dari 1,5 juta, kira-kira 500-600an ribu. Kalau kemudian
            mereka tertular di daerah tadi yang tidak ada ini (imunisasi), tahun
            depan kita sudah panen dong dengan rubella congenital syndrome,
            meski segala sesuatunya tidak head to head," lanjutnya.
          </p>
          <p>Rubella congenital syndrome sendiri akan membuat bayi lahir dengan
            cacat, bahkan bisa menimbulkan masalah yang cukup serius dan
            kompleks, seperti kelainan jantung, katarak, masalah pada otak, dan
            masalah pada tumbuh kembangnya.</p>
          <p>Anung menambahkan, jika masyrakat tidak mau melakukan imunisasi MR
            pada anaknya, maka harus bersedia menanggung semua dampak yang akan
            terjadi nantinya.</p>
          <p>"Kalau kamu disediain minum sehat, tapi milih air comberan, kamu
            kalau sakit nggak usah sambat saya. Itu harus disampaikan secara
            konkret meski persuasif. Nggak boleh orang egois hanya dengan
            pendapatnya sendiri," tegasnya.</p>
          <p>"Terus siapa yang harus menyadarkan? Kita semuanya. Anda yang
            membuat sebuah komunitas nih harus sadar, eh inget dong kalau kamu
            hanya satu saja yang diimunisasi, tahu nggak kamu berpotensi
            menularkan ke saya, tapi juga sebaliknya kamu berpotensi tidak jadi
            seperti saya," tutupnya.</p>'
	],	
	[
		'slug'=>'kontroversi-vaksin-mr-kenali-manfaat-vaksin-dan-bahaya-virusnya',
		'title'=>'Kontroversi Vaksin MR, Kenali Manfaat Vaksin dan Bahaya Virusnya',
		'reference'=>'Tempo.co',
		'image'=>'assets/images/berita-4.jpg',
		'link_source'=>'https://gaya.tempo.co/read/1133958/kontroversi-vaksin-mr-kenali-manfaat-vaksin-dan-bahaya-virusnya',
		'date'=>'Senin, 8 Okt 2018 05:45 WIB',
		'author'=>'Mitra Tarigan - gaya.tempo.co',
		'content'=>'<p>
	            TEMPO.CO, Jakarta - Kontroversi vaksin MR memanas sepanjang tahun
	            ini. Majelis Ulama Indonesia (MUI) lewat situs web resminya
	            menerangkan, penggunaan vaksin Measles Rubella alias MR produk dari
	            Serum Institute of India (SII) saat ini dibolehkan (mubah) karena
	            kondisi keterpaksaan (darurat syar’iyyah) dan belum ditemukan vaksin
	            MR yang halal dan suci. Meski demikian kontroversi tetap saja
	            meruncing. Wali Kota Jambi, Syarif Fasha, sampai melontarkan
	            pernyataan siap menanggung kesalahan akibat pemberian vaksin MR.
	            Yang bikin miris, masih banyak masyarakat yang belum 100 persen
	            paham apa vaksin MR itu.
	          </p>
	          <p>
	            Spesialis anak dari Poliklinik Advance RSIA Bunda Jakarta, Abdullah
	            Reza, menerangkan imunisasi dan vaksinasi sebenarnya dua hal
	            berbeda. Imunisasi adalah proses membuat tubuh menjadi imun, salah
	            satu caranya dengan vaksinasi. Vaksinasi merupakan proses membuat
	            imun secara aktif. Belakangan masyarakat menyebut vaksinasi sebagai
	            imunisasi untuk menyederhanakan istilah. Prinsip dasar vaksinasi
	            adalah latihan. Tubuh kita ibarat negara.
	          </p>
	          <p>
	            “Saat negara dalam kondisi aman, tentara tetap latihan perang. Saat
	            negara terancam, tentara melawan berdasarkan strategi latihan yang
	            selama ini mereka jalani. Dalam kondisi terdesak bukan tidak mungkin
	            warga sipil dipersenjatai untuk membantu perang. Peluang sipil kalah
	            tentu lebih besar. Seperti itulah vaksinasi. Tentara yang berlatih
	            ibarat anak yang divaksin. Mereka lebih siap melawan penyakit,” kata
	            Abdullah kepada Bintang, di Jakarta, pekan lalu.
	          </p>
	          <p>
	            Anak yang divaksin memang tidak 100 persen kebal terhadap penyakit.
	            Saat kena serangan virus atau bakteri, mereka lebih siap dan proses
	            pemulihannya lebih cepat. Vaksinasi menguatkan daya tahan tubuh si
	            kecil 85 sampai 95 persen. Vaksinasi ada tiga tipe. Pertama,
	            memasukkan virus yang telah dilemahkan ke dalam tubuh. Kedua,
	            memasukkan bagian virus ke dalam tubuh. Ketiga, memberi toksin alias
	            racun ke tubuh. Vaksin MR diberikan dengan teknik pertama.
	          </p>
	          <p>
	            Abdullah menerangkan MR singkatan dari Measles Rubella. Nama Measles
	            Rubella adalah infeksi yang disebabkan virus. Gejala measles dan
	            rubela mirip, yakni demam dan muncul ruam di kulit. Ruam pertanda
	            kulit telah terkelupas. Masalahnya pada campak, yang terkelupas
	            bukan hanya kulit tubuh. “Pengelupasan bisa terjadi pada kulit
	            saluran pernapasan, itu sebabnya campak menjelma jadi batuk, pilek,
	            dan pneunomia (radang paru-paru). Pengelupasan kulit saluran
	            pencernaan juga bisa, kemudian disusul diare. Kulit selaput otak
	            juga bisa mengelupas dan memicu radang otak. Campak itu berat. Orang
	            bilang infeksi rubela lebih ringan ketimbang campak,” kata dia.
	          </p>
	          <p>
	            Abdullah menambahkan, “Bukan berarti rubela bisa disepelekan. Yang
	            dikhawatirkan para dokter, anak yang terkena rubela bisa menularkan
	            virus kepada ibu hamil di sekitarnya. Kemudian memicu terjadinya
	            teratogenik yakni kecacatan janin yang terjadi pada trimester
	            pertama seperti kelainan jantung, kelainan mata, dan otak janin
	            terjangkit mikrosefalus.”
	          </p>
	          <p>
	            Selama ini, pemerintah Indonesia memberikan imunisasi campak, bukan
	            MR. Hasilnya, jumlah kasus campak menurun namun kasus rubela
	            meninggi.
	          </p>
	          <p>
	            Indonesia gencar mengampanyekan vaksin MR. Fase pertama dimulai
	            Agustus dan September 2017, menyasar Jakarta dan seluruh pulau Jawa
	            untuk anak usia 9 bulan sampai 15 tahun. Fase kedua dimulai Agustus
	            hingga September tahun ini untuk anak-anak di luar Pulau Jawa.
	          </p>
	          <p>
	            “Setelah kedua fase ini selesai, vaksin MR akan menjadi agenda wajib
	            bagi balita usia 9 bulan dan 18 bulan. Setelah mendapat vaksin MR,
	            Anda disarankan memberikan vaksin MR lagi atau vaksin MMR (Measles,
	            Mumps, dan Rubella) agar si kecil terhindar dari penyakit gondong,
	            campak, dan rubela. Saya merekomendasikan vaksin MMR,” Abdullah
	            menukas.
	          </p>
	          <p>
	            Sayangnya, di Indonesia belum ada. Negara terdekat yang sudah
	            memiliki layanan vaksin MMR yakni Malaysia dan Singapura. Biaya
	            vaksin di Malaysia, menurutnya, lebih murah daripada Negeri Singa.
	            “Vaksin MMR sangat penting. Ingat, 40 persen anak laki-laki yang
	            terkena penyakit gondong testisnya bermasalah. Berdasarkan
	            penelitian, 90 persen laki-laki yang testisnya terdampak gondong
	            menjadi mandul,” Abdullah mengingatkan.
	          </p>'
	],
	[
		'slug'=>'dokter-jelaskan-beda-vaksinasi-dan-imunisasi-termasuk-vaksin-mr',
		'title'=>'Dokter Jelaskan beda Vaksinasi dan Imunisasi, Termasuk Vaksin MR',
		'reference'=>'Tempo.co',
		'image'=>'assets/images/berita-5.jpg',
		'link_source'=>'https://cantik.tempo.co/read/1133941/dokter-jelaskan-beda-vaksinasi-dan-imunisasi-termasuk-vaksin-mr',
		'date'=>'Minggu, 7 Okt 2018 20:38 WIB',
		'author'=>'Yayuk Widiyarti - cantik.tempo.co',
		'content'=>'<p>
            TEMPO.CO, Jakarta - Kontroversi vaksin MR (measles rubella) memanas
            sepanjang 2018. Majelis Ulama Indonesia (MUI) lewat situs resminya
            menerangkan penggunaan vaksin MR produk dari Serum Institute of
            India (SII) saat ini dibolehkan (mubah) karena kondisi keterpaksaan
            (darurat syar’iyyah) dan belum ditemukan vaksin MR yang halal dan
            suci.
          </p>
          <p>
            Meski demikian, kontroversi tetap saja meruncing. Walikota Jambi,
            Syarif Fasha, sampai melontarkan pernyataan siap menanggung
            kesalahan akibat pemberian vaksin MR. Yang bikin miris, masih banyak
            masyarakat yang belum 100 persen paham apa vaksin MR itu.
          </p>
          <p>
            Spesialis anak dari Poliklinik Advance RSIA Bunda Jakarta, dr.
            Abdullah Reza, SpA., menerangkan imunisasi dan vaksinasi sebenarnya
            dua hal berbeda. Imunisasi adalah proses membuat tubuh menjadi imun,
            salah satu caranya dengan vaksinasi.
          </p>
          <p>
            Vaksinasi merupakan proses membuat imun secara aktif. Belakangan,
            masyarakat menyebut vaksinasi sebagai imunisasi untuk
            menyederhanakan istilah. Prinsip dasar vaksinasi adalah latihan,
            tubuh kita ibarat negara.
          </p>
          <p>
            “Saat negara dalam kondisi aman, tentara tetap latihan perang. Saat
            negara terancam, tentara melawan berdasarkan strategi latihan yang
            selama ini mereka jalani,” jelasnya.
          </p>
          <p>
            “Dalam kondisi terdesak, bukan tidak mungkin warga sipil
            dipersenjatai untuk membantu perang. Peluang sipil kalah tentu lebih
            besar. Seperti itulah vaksinasi. Tentara yang berlatih ibarat anak
            yang divaksin. Mereka lebih siap melawan penyakit,” tambah Abdullah.
          </p>
          <p>
            Anak yang divaksin memang tidak 100 persen kebal terhadap penyakit.
            Saat kena serangan virus atau bakteri, mereka lebih siap dan proses
            pemulihannya lebih cepat.
          </p>
          <p>
            Vaksinasi menguatkan daya tahan tubuh si kecil 85 sampai 95 persen.
            Vaksinasi ada tiga tipe. Pertama, memasukkan virus yang telah
            dilemahkan ke dalam tubuh. Kedua, memasukkan bagian virus ke dalam
            tubuh. Ketiga, memberi toksin alias racun ke tubuh. Vaksin MR
            diberikan dengan teknik pertama.
          </p>'
	]
];