function openNav() {
  document.getElementById("myNav").style.width = screen.width <= 450 ? "80%":"30%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

function setDescBg() {
  var descBackgroundHeight = document.getElementById("content-desc");
  var footerHeight = document.getElementById("footer").offsetHeight;

  if (descBackgroundHeight != null) {
    document.getElementById("content-desc-bg").style.height = footerHeight + descBackgroundHeight.offsetHeight + "px";
  }
}
