<?php define('imunisasi', 'imunisasimr2020');
  include 'ref-berita.php';

  $color = 'navbar-white';
  include 'header.php';  

  $index = find($_GET['s'],$news);
  $berita = $news[$index];

  $title = $berita['title'];

?>

<div class="detail-berita mb-4 container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="wrapper">
        <img src="<?= $berita['image']; ?>" alt="<?= $berita['title']; ?>" />
      </div>
      <small>(Sumber: <a href="<?= getHostUrl($berita['link_source']); ?>"><?= getHostUrl($berita['link_source']); ?></a>)</small>
      <small
        ><?= $berita['date']; ?><br /><?= $berita['author']; ?></small
      >
      <h4><?= $berita['title']; ?></h4>
      <label><?= $berita['reference']; ?></label>
      <?= $berita['content']; ?>
    </div>
  </div>
</div>

<?php include 'footer.php'; ?>