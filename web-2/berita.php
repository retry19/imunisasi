<?php 
  define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  
  $title = 'Berita';
  $color = 'navbar-white';
  include 'header.php';
?>

<div class="berita container">
  <div class="row justify-content-center">
    <?php foreach ($news as $row) 
      {
        echo '<div class="col-md-4 mb-4">
                <a href="berita-detail.php?s='.$row['slug'].'">
                  <div class="wrapper mb-3">
                    <img src="'.$row['image'].'" alt="'.$row['title'].'" />
                  </div>
                  <p>'.$row['title'].'</p>
                  <label class="mt-1">'.$row['reference'].'</label>
                </a>
              </div>';
      }
    ?>
  </div>
</div>

<?php include 'footer.php' ?>