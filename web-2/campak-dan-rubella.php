<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Campak dan Rubella';
  $color = 'navbar-white';
  include 'header.php';
?>

<!-- start content -->
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-title">
      <h2>Campak dan Rubella itu apa?</h2>
      <div class="content-desc" id="content-desc">
      <div class="content-desc-bg" id="content-desc-bg"></div>
        <p>
          Campak merupakan penyakit yang dapat disebabkan oleh virus yang
          dapat ditularkan melalui bersin dan batuk penderita. Campak
          termasuk penyakit yang mudah menular. Gejalanya berupa demam yang
          tinggi dan bercak kemerahan pada kulit atau rash juga biasanya
          disertai batuk pilek. Gejalanya dapat berbahaya jika disertai
          dengan penyakit lain seperti Pneumonia, Diare, dan Meningitis
          karena bisa berakhir pada kematian. Ketika penderita Campak
          berinteraksi erat dengan orang yang bukan penderita memiliki
          potensi 90% untuk tertular. Namun hal tersebut dapat diatasi
          dengan imunisasi agar tidak terserang virus Campak.
        </p>
        <p>
          Sedangkan penyakit Rubella merupakan penyakit ringan, kelompok
          usia yang rentan mengalami infeksi Rubella adalah anak dan dewasa
          muda. Namun infeksi Rubella yang harus diwaspadai dalam kesehatan
          masyarakat adalah infeksi yang menjangkit wanita hamil. Infeksi
          Rubella yang menyerang wanita hamil yang pada umumnya menjangkit
          saat trimester pertama. Infeksi Rubella pada wanita hamil dapat
          berdampak kepada janin atau bayi yang dilahirkan yang menyebabkan
          keguguran atau kecacatan yang disebabkan oleh Sindrom Rubella
          Kongenital atau biasa disebut CRS.
        </p>
        <p>
          Penyakit ini memiliki potensi menjadi wabah apabila imunisasi
          Campak dan Rubella cakupannya rendah dan tidak terbentuknya
          kekebalan kelompok atau herd immunity.
        </p>
      </div>
    </div>
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/campak-dan-rubella.jpg"
          alt="Ilustrasi Campak dan Rubella"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi Campak dan Rubella</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a href="https://www.honestdocs.id"
          >https://www.honestdocs.id</a
        >
        )</small
      >
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer-2.php' ?>