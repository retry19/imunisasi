<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Epidemiologi';
  $color = 'navbar-white';
  include 'header.php';
?>

<!-- start content -->
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-title">
      <h2>Lalu bagaimana penularan Campak dan Rubella?</h2>
      <div class="content-desc" id="content-desc">
      <div class="content-desc-bg" id="content-desc-bg"></div>
        <p>
          Campak yang biasa disebut dengan morbili atau measles adalah penyakit yang disebabkan oleh virus serta mudah menular atau infeksius. Sebelum adanya imunisasi Campak pada tahun 1980 diperkiran lebih dari 20 juta orang di dunia terdampak penyakit Campak dan 2,6 juta orang mengalami kematian setiap tahunnya. Kelompok usia yang mendominasi terkena Campak didominasi oleh anak yang berusia dibawah 5 tahun. Namun saat telah dimulai vaksinasi melalui program imunisasi pada tahun 2000 yang dilakukan di negara-negara yang berisko tinggi termasuk Indonesia sebagai negara yang memiliki kasus penularan terbesar hingga tahun 2012 angka kematian yang diakibatkan oleh Campak secara gobal mengalami penuranan hingga 78%.
        </p>
        <p>
          Sedangkan Rubella adalah penyakit yang disebabkan oleh togavirus yang berjenis rubivirus yang termasuk pada golongan virus RNA. Virus Rubella merupakan virus yang mudah mati jika terkena sinar ultra violet, bahan kimia, bahan asam, dan pemanasan. Rubella dapat menular lewat saluran pernapasan yang disebabkan oleh bersin dan batuk penderita. Virus Rubella berkembang biak di bagian nasofaring dan kelenjar getah bening serta menginfeksi tubuh dalam rentang waktu 4 sampai 7 hari terhitung sejak virus masuk kedalam tubuh. Namun masa inkubasinya 14 sampai 21 hari. Gejalanya  dapat berupa: 
        </p>
        <p>
          <ol type="a" class="pl-4">
            <li>Demam ringan dengan suhu tubuh 37,2 derajat celcius serta bercak merah dengan dibarengi gejala lain seperti pembesaran kelenjar limfe di sub occipital, belakang telinga, dan leher belakang.</li>
            <li>Gejala yang terjadi jika menjangkit kepada anak dapat berupa gejala demam secara ringan atau tidak terjadi gejala sama sekali.</li>
            <li>
              Sedangkan gejala pada wanita hamil dapat menimbulkan peradangan serta kekakuan pada sendi yang biasa disebut arthritis atau arthralgia. Penyakit Rubella yang menjangkit pada ibu hamil dan janin yang terjadi saat kehamilan trimester pertama bisa berdampak keguguran pada kandungan atau bayi lahir dengan kelainan Congenital Rubella Syndrome atau CRS. Kelainan CRS bisa berupa: 
              <ul class="pl-4">
                <li>Kelainan pada jantung <br>Bisa berupa stenosis katup pulmonal, defek septum atrial, defek septum ventrikel, dan defek septum atrial.</li>
                <li>Kelainan pada mata <br>Bisa berupa katarak kogenital, galukoma kogenital, dan pigmentary retinopati.</li>
                <li>Kelainan pada pendengaran</li>
                <li>Kelainan pada sistem saraf pusat <br>Bisa berupa retardasi mental, mikrocephalia, meningoesefalitis.</li>
                <li>Serata kelainan lainnya <br>Bisa berupa radioluscent bone, puroura, splenomegali, dan ikterik yang muncul dalam 24 jam setelah lahir.</li>
              </ul>
            </li>
          </ol>
        </p>
      </div>
    </div>
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/epidemiologi.jpg"
          alt="Ilustrasi Epidemiologi"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi Epidemiologi</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a
          href="https://tirto.id"
          >https://tirto.id</a
        >
        )</small
      >
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer-2.php' ?>