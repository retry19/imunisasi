<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Fatwa MUI';
  $color = 'navbar-white';
  include 'header.php';
?>

<!-- start content -->
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-title">
      <h2>Tahukah Anda penggunaan Vaksin MR juga diatur dalam Fatwa MUI?</h2>
      <div class="content-desc" id="content-desc">
      <div class="content-desc-bg" id="content-desc-bg"></div>
        <p>
          Penggunaan vaksin MR yang merupakan produk dari Serum Institute of
          India (SII) diperbolehkan dalam
          <strong>Fatwa MUI Nomor 33 Tahun 2018</strong> setidaknya
          dikarenakan 3 hal yaitu:
        </p>
        <p>
          <ul class="pl-4">
            <li>Ada kondisi keterpaksaan (darurat syar’iyyah)</li>
            <li>Belum ditemukan vaksin MR yang halal dan suci</li>
            <li>Ada keterangan dari ahli yang kompeten dan dipercaya tentang bahaya yang ditimbulkan akibat tidak diimunisasi dan belum adanya vaksin yang halal.</li>
          </ul>
        </p>
        <p>Salah satu yang menjadi acuan keputusan ini adalah Al-Quran surat Al-An’am ayat 119 yang berbunyi:</p>
        <p class="text-right content-quran">وَمَا لَكُمْ أَلَّا تَأْكُلُوا۟ مِمَّا ذُكِرَ ٱسْمُ ٱللَّهِ عَلَيْهِ وَقَدْ فَصَّلَ لَكُم مَّا حَرَّمَ عَلَيْكُمْ إِلَّا مَا ٱضْطُرِرْتُمْ إِلَيْهِ  وَإِنَّ كَثِيرًا لَّيُضِلُّونَ بِأَهْوَآئِهِم بِغَيْرِ عِلْمٍ ۗ إِنَّ رَبَّكَ هُوَ أَعْلَمُ بِٱلْمُعْتَدِينَ
        </p>
        <p>
          Arab-Latin: 
          <em>Wa mā lakum allā ta`kulụ mimmā żukirasmullāhi 'alaihi wa qad faṣṣala lakum mā ḥarrama 'alaikum illā maḍṭurirtum ilaīh, wa inna kaṡīral layuḍillụna bi`ahwā`ihim bigairi 'ilm, inna rabbaka huwa a'lamu bil-mu'tadīn</em>
        </p>
        <p>
          Terjemah Arti: Mengapa kamu tidak mau memakan (binatang-binatang yang halal) yang disebut nama Allah ketika menyembelihnya, padahal sesungguhnya Allah telah menjelaskan kepada kamu apa yang diharamkan-Nya atasmu, kecuali apa yang terpaksa kamu memakannya. Dan sesungguhnya kebanyakan (dari manusia) benar benar hendak menyesatkan (orang lain) dengan hawa nafsu mereka tanpa pengetahuan. Sesungguhnya Tuhanmu, Dialah yang lebih mengetahui orang-orang yang melampaui batas.
        </p>
        <p>Fatwa selengkapnya dapat Anda unduh pada tombol dibawah ini.</p>
        <p class="text-center">
          <a href="assets/pdf/Fatwa-MUI-No.-33-Tahun-2018-tentang-penggunaan-vaksin-MR-measles-rubella-produksi-dari-SII-serum-institue-of-India-untuk-imunisasi.pdf" class="btn btn-primary btn-custom" download target="_blank">Unduh</a>
        </p>
      </div>
    </div>
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/fatwa-mui.jpg"
          alt="Kantor MUI Pusat"
          class="img-fluid"
        />
      </div>
      <small>Foto: Kantor MUI Pusat</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a
          href="https://www.cendananews.com"
          >https://www.cendananews.com</a
        >
        )</small
      >
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer-2.php' ?>