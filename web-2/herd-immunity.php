<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Herd Immunity';
  $color = 'navbar-white';
  include 'header.php';
?>

<!-- start content -->
<div class="container">
      <div class="row content">
        <div class="col-md-6 content-title">
          <h2>Apa itu Herd Immunity?</h2>
          <div class="content-desc" id="content-desc">
            <div class="content-desc-bg" id="content-desc-bg"></div>
            <p>
              Herd immunity dalam bahasa indonesia berarti kekebalan kelompok
              atau komunitas. Herd immunity dari suatu penyakit dapat terbentuk
              dengan cara pemberian vaksin cakupannya secara meluas atau herd
              immunity dapat tercapai melalui kekebalan alami yang terbentuk
              pada sebagian besar orang karena pernah terinfeksi penyakit
              tertentu lalu sembuh dan memiliki antibodi yang mencegahnya
              penyakit tersebut terinfeksi kembali.
            </p>
            <p>
              Jadi cara kerja konsep herd immunity adalah jika suatu kelompok
              masyarakat memiliki kekebalan terhadap suatu virus dari penyakit
              tertentu, maka virus tersebut akan hilang dengan sendirinya.
              Konsep herd immunity merupakan konsep dimana orang-orang sehat
              melindungi orang-orang disekitarnya yang sistem imunnya kurang
              bagus atau yang tidak bisa menerima vaksin, sehingga orang-orang
              yang imunnya kurang bagus atau yang tidak bisa menerima vaksin
              tidak mudah tertular penyakit tertentu.
            </p>
          </div>
        </div>
        <div class="col-md-6 content-img">
          <div class="wrapper">
            <img
              src="assets/images/herd-immunity.jpg"
              alt="Ilustrasi Herd Immunity"
              class="img-fluid"
            />
          </div>
          <small>Foto: Ilustrasi Herd Immunity</small><br />
          <small style="font-size: 0.6rem;"
            >(Sumber:
            <a
              href="https://myeatandtravelstory.wordpress.com"
              >https://myeatandtravelstory.wordpress.com</a
            >
            )</small
          >
        </div>
      </div>
    </div>
    <!-- end content -->

<?php include 'footer-2.php' ?>