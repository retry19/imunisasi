<?php 
  define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';

  $title = 'Home';
  include 'header.php';
?>

<!-- start banner -->
<div class="row banner text-white">
  <div class="col-lg-7 banner-text">
    <div class="row">
      <div class="col-md-12">
        <h1>
          Jangan sampai buah hati terkena bahaya dari penyakit campak &
          rubella.
        </h1>
      </div>
    </div>
    <div class="row berita-main">
      <div class="col-md-12">
        <h2>Berita</h2>
        <div class="row">
          <?php for ($i=0; $i<2; $i++)
            {
              $berita = $news[$i];
              echo '<div class="col-md-6">
                      <a href="berita-detail.php?s='.$berita['slug'].'">
                        <div class="berita-main-wrapper">
                          <img
                            src="'.$berita['image'].'"
                            class="img-fluid mb-3"
                            alt="'.$berita['title'].'"
                          />
                        </div>
                        <p>'.$berita['title'].'</p>
                      </a>
                    </div>';
            }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-5 banner-image">
    <img
      src="assets/images/main-photo.png"
      alt="Foto Ibu dan Anak"
      class="img-fluid"
    />
    <small>(Sumber: https://www.pexels.com)</small>
  </div>
</div>
<!-- end banner -->

<?php include 'footer.php' ?>