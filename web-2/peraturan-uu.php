<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Peraturan UU';
  $color = 'navbar-white';
  include 'header.php';
?>

<!-- start content -->
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-title">
      <h2>Tahukah Anda landasan hukum pelaksanaan imunisasi?</h2>
      <div class="content-desc" id="content-desc">
      <div class="content-desc-bg" id="content-desc-bg"></div>
        <p>
          Berikut ini merupakan landasan hukum imunisasi.
        </p>
        <p>
          <img
            src="assets/images/landasan-hukum.jpg"
            alt="Landasan hukum imunisasi"
            class="img-fluid"
          />
        </p>
      </div>
    </div>
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/peraturan-uu.jpg"
          alt="Ilustrasi landasan hukum"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi landasan hukum</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a
          href="https://www.acq-intl.com"
          >https://www.acq-intl.com</a
        >
        )</small
      >
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer-2.php' ?>