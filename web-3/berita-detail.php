<?php define('imunisasi', 'imunisasimr2020');
  include 'ref-berita.php';

  include 'header.php';  

  $index = find($_GET['s'],$news);
  $berita = $news[$index];

  $title = $berita['title'];

?>

<!-- start content -->
<div class="row banner-content banner-content-berita"></div>
<div class="container detail-berita">
  <div class="row justify-content-center">
    <div class="col-md-8 mb-5">
      <div class="wrapper mb-3">
        <img src="<?= $berita['image']; ?>" alt="<?= $berita['title']; ?>" />
      </div>
      <small>(Sumber: <a href="<?php echo getHostUrl($berita['link_source']); ?>"><?php echo getHostUrl($berita['link_source']); ?></a>)</small>
      <small
        ><?= $berita['date']; ?><br /><?= $berita['author']; ?></small
      >
      <h4><?= $berita['title']; ?></h4>
      <label><?= $berita['reference']; ?></label>
      <?= $berita['content']; ?>
    </div>
  </div>
</div>
  <!-- end content -->

<?php include 'footer.php'; ?>