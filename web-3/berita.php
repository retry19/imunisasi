<?php 
  define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  $title = 'Berita';
  include 'header.php';
?>

<!-- start content -->
<div class="row banner-content text-white">
  <div class="col-md-12 text-center">
    <h2>Berita</h2>
  </div>
</div>
<div class="container berita">
  <div class="row justify-content-center">
    <?php foreach($news as $row) 
      {
        echo '<div class="col-md-12 mb-4">
                <a href="berita-detail.php?s='.$row['slug'].'">
                  <div class="wrapper mb-3">
                    <img src="'.$row['image'].'" alt="'.$row['title'].'" />
                  </div>
                  <p>'.$row['title'].'</p>
                  <label class="mt-1">'.$row['reference'].'</label>
                </a>
              </div>';
      }
    ?>
  </div>
</div>
<!-- end content -->

<?php include 'footer.php' ?>