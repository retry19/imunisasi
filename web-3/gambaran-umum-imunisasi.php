<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Gambaran Umum Imunisasi';
  include 'header.php';
?>

<!-- start content -->
<div class="row banner-content text-white">
  <div class="col-md-12 text-center p-5">
    <h2>Imunisasi itu apa sih?</h2>
  </div>
</div>
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/gambaran-umum-imunisasi.jpg"
          alt="Ilustrasi Imunisasi"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi Imunisasi</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a
          href="https://aktual.com"
          >https://aktual.com</a
        >
        )</small
      >
    </div>
    <div class="col-md-6 content-desc">
      <p>
        Imunisasi adalah suatu tindakan preventif dalam mencegah terjadinya penularan Penyakit yang Dapat Dicegah Dengan Imunisasi atau PD3I. Dengan cara menyutikan vaksin ke dalam tubuh seseorang. Sehingga memungkinkan orang tersebut kebal atau resisten dengan penyakit tertentu. Berdasarkan definisi tersebut, apabila ada anak yang diimunisasi, maka anak tersebut diberikan kekebalan terhadap PD3I. Imunisasi bertujuan untuk menekan angka kesakitan, kecacatan, serta kematian yang diakibatkan oleh penyakit yang dapat dicegah melalui imunisasi. Penyakit yang dapat dicegah melalui imunisasi diantaranya adalah:
      </p>
      <p>
        <ul class="pl-4">
          <li>Difteri</li>
          <li>Pertusis</li>
          <li>Tetanus</li>
          <li>Tuberculosis (TBC)</li>
          <li>Campak</li>
          <li>Poliomielitis</li>
          <li>Hepatitis B</li>
          <li>Hemofilus Influenza tipe b (Hib)</li>
          <li>HPV (Human papiloma virus)</li>
          <li>Hepatitis A</li>
        </ul>
      </p>
      <p>
        Sedangkan vaksin merupakan suatu zat yang dimasukan kedalam tubuh pada saat imunisasi yang berupa antigen yang terdiri dari mikroorganisme yang sudah mati, masih hidup namun dilemahkan, masih utuh atau sebagiannya. Mikroorganisme yang sudah diolah atau toksin mikroorganisme yang sudah diolah lalu menjadi toksoid atau protein rekombinan. Lalu mikroorganisme tersebut diberikan kepada seseorang sehingga seseorang dapat memiliki kekebalan yang spesifik terhadap penyakit tertentu. Berdasarkan definisi tersebut vaksin merupakan antigen yang berupa mikroorganisme yang diolah sedemikian rupa, bermanfaat untuk merangsang kekebalan pada tubuh seseorang terhadap penyakit tertentu. Dibawah ini merupakan program imunisasi dalam Permenkes No. 12 Tahun 2017.
      </p>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-md-12 text-center">
      <img src="assets/images/program-imunisasi.jpg" alt="Program Imunisasi" class="img-fluid">
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer.php' ?>