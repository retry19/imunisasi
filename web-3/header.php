<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="shortcut icon"
      href="assets/icons/logo-01.png"
      type="image/x-icon"
    />
    <link rel="stylesheet" href="assets/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/styles/app.css" />
    <title><?php if(isset($title)) { echo $title.' :: Ayo Imunisasi MR'; } else { echo 'Ayo Imunisasi MR'; } ?></title>
  </head>
  <body>
    <!-- start navbar -->
    <nav class="navbar navbar-light bg-transparent fixed-top">
      <div class="container">
        <a class="navbar-brand mx-auto" href="./index.php"
          ><img
            src="assets/icons/logo2-01.png"
            alt="Logo Ayo Imunisasi MR"
            height="40"
        /></a>
        <button class="navbar-toggler" type="button" onclick="openNav()">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="overlay" id="myNav">
          <div class="overlay-brand">
            <img
              src="assets/icons/logo2-01.png"
              alt="Logo Ayo Imunisasi MR"
              height="40"
            />
          </div>
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"
            >&times;</a
          >
          <ul class="overlay-content mr-0 ml-auto">
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="pengetahuanUmum"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Pengetahuan Umum
              </a>
              <div class="dropdown-menu" aria-labelledby="pengetahuanUmum">
                <a class="dropdown-item" href="./herd-immunity.php"
                  >Herd Immunity</a
                >
                <a class="dropdown-item" href="./gambaran-umum-imunisasi.php"
                  >Gambaran Umum Imunisasi</a
                >
                <a
                  class="dropdown-item"
                  href="./perbedaan-imunisasi-dan-vaksinasi.php"
                  >Perbedaan Imunisasi dan Vaksinasi</a
                >
              </div>
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="imunisasiMR"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Imunisasi MR
              </a>
              <div class="dropdown-menu" aria-labelledby="imunisasiMR">
                <a class="dropdown-item" href="./campak-dan-rubella.php"
                  >Campak dan Rubella</a
                >
                <a class="dropdown-item" href="./epidemiologi.php"
                  >Epidemiologi</a
                >
                <a class="dropdown-item" href="./vaksin-mr.php">Vaksin MR</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="landasanHukum"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Landasan Hukum
              </a>
              <div class="dropdown-menu" aria-labelledby="landasanHukum">
                <a class="dropdown-item" href="./peraturan-uu.php"
                  >Peraturan UU</a
                >
                <a class="dropdown-item" href="./fatwa-mui.php">Fatwa MUI</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./berita.php">
                Berita
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- end navbar -->