<?php 
  define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';

  $title = 'Home';
  include 'header.php';
?>

<!-- start banner -->
<div class="row banner text-white">
  <div class="col-md-12 banner-text">
    <h1>
      Kesehatan anak adalah segalanya.
    </h1>
    <img
      src="assets/images/main-photo.png"
      alt="Foto Ibu dan Anak"
      class="img-fluid"
    />
    <small>(Sumber: https://www.pexels.com)</small>
  </div>
</div>
<!-- end banner -->
<div class="container">
  <div class="row berita-main">
    <div class="col-md-12">
      <h2>Berita</h2>
      <div class="row">
        <?php for ($i=0; $i<3; $i++)
          {
            $berita = $news[$i];
            echo '<div class="col-md-4">
                    <a href="berita-detail.php?s='.$berita['slug'].'">
                      <div class="berita-main-wrapper">
                        <img
                          src="'.$berita['image'].'"
                          class="img-fluid mb-3"
                          alt="'.$berita['title'].'"
                        />
                      </div>
                      <p>'.$berita['title'].'</p>
                    </a>
                  </div>';
          }
        ?>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php' ?>