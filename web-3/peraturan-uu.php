<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Peraturan UU';
  include 'header.php';
?>

<!-- start content -->
<div class="row banner-content text-white">
  <div class="col-md-12 text-center p-5">
    <h2>Tahukah Anda landasan hukum pelaksanaan imunisasi?</h2>
  </div>
</div>
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/peraturan-uu.jpg"
          alt="Ilustrasi Landasan Hukum"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi landasan hukum</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a
          href="https://www.acq-intl.com"
          >https://www.acq-intl.com</a
        >
        )</small
      >
    </div>
    <div class="col-md-6 content-desc">
      <p>
        Berikut ini merupakan landasan hukum imunisasi.
      </p>
      <p>
        <img
          src="assets/images/landasan-hukum.jpg"
          alt="Landasan hukum imunisasi"
          class="img-fluid"
        />
      </p>
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer.php' ?>