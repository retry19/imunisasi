<?php 
  define('imunisasi', 'imunisasimr2020');

  $title = 'Vaksin MR';
  include 'header.php';
?>

<!-- start content -->
<div class="row banner-content text-white">
  <div class="col-md-12 text-center p-5">
    <h2>Apa dan bagaimana manfaat dari Vaksin MR?</h2>
  </div>
</div>
<div class="container">
  <div class="row content">
    <div class="col-md-6 content-img">
      <div class="wrapper">
        <img
          src="assets/images/vaksin-mr.jpg"
          alt="Ilustrasi Vaksin Campak dan Rubella"
          class="img-fluid"
        />
      </div>
      <small>Foto: Ilustrasi Vaksin Campak dan Rubella</small><br />
      <small style="font-size: 0.6rem;"
        >(Sumber:
        <a href="https://mediaindonesia.com"
          >https://mediaindonesia.com</a
        >
        )</small
      >
    </div>
    <div class="col-md-6 content-desc">
      <p>
        Vaksin Measles Rubella atau vaksin MR merupakan klasifikasi vaksin hidup yang dilemahkan atau live attenuated yang berbentuk serbuk kering yang berwarna putih serta kekuningan untuk menggunakannya dibutuhkan pelarut serta pengencer yang disediakan oleh produsen vaksin yang sama. Kemasan vaksin terdiri dari 10 dosis per vial. Setiap dosis vaksin MR tersebut mengandung:
      </p>
      <p>
        <ul class="pl-4">
          <li>Virus Campak sejumlah 1000 CCID50</li>
          <li>Virus Rubella sejumlah 1000 CCID50</li>
        </ul>
      </p>
      <p>
        Imunisasi campak dan rubella dapat memberikan manfaat untuk melindungi anak dari kecacatan dan kematian yang diakibatkan oleh pneumonia, diare, kerusakan otak, ketulian, kebutaan dan penyakit jantung bawaan.
      </p>
      <p>
        Vaksin MR juga sangat aman digunakan dan berkualitas karena telah mempun yai ijin edar dari Badan Pengawas Obat dan Makanan atau BPOM. Selain itu mendapatkan pra qualifikasi dari badan kesehatan dunia yaitu World Health Organization atau WHO dan telah digunakan sejak tahun 1989 lebih dari 140 negara termasuk negara berpenduduk Muslim.
      </p>
    </div>
  </div>
</div>
<!-- end content -->

<?php include 'footer.php' ?>