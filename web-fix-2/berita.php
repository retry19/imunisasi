<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  
  $title = 'Berita';
  $navbar = 'pink';
  include 'header.php';
?>

<div class="berita">
  <img class="icon-love" src="assets/icons/love-berita.png" alt="icon love" style="margin-top: 100px;max-width: 400px;margin-right: 20px;">
  <div class="row justify-content-center">
    <?php 
      foreach ($news as $i => $row) {
        if (($i+2)%2==0) {
          echo '</div><div class="row justify-content-center">';
        }
        echo '<div class="col-md-6 mb-4" data-aos="fade-in" data-aos-delay="'.($i+3).'00">
                <a href="berita-detail.php?s='.$row['slug'].'">
                  <div class="wrapper mb-3">
                    <img src="'.$row['image'].'" alt="'.$row['title'].'" />
                  </div>
                  <p>'.$row['title'].'</p>
                  <label class="mt-1">'.$row['reference'].'</label>
                </a>
              </div>';
      }
    ?> 
  </div>
</div>

<?php include 'footer.php'; ?>