<?php define('imunisasi', 'imunisasimr2020');

  $title = 'FAQ';
  $navbar = 'pink';
  include 'header.php';
?>
<style type="text/css">
  .card .card-header img.icon-faq {
    max-width: 2rem;
    transform: rotate(180deg);
  }
  .card .card-header.collapsed img.icon-faq {
    transform: none;
  }
  .card .card-header .btn-link{
    text-align: left;
  }
  .card .card-header h2 {
    font-size: 1rem;
    color: #feafce;
  }
  .card .card-header {
    cursor: pointer;
  }
</style>
<?php 
$faq = [
  [
    'title'=>'1. Produk vaksin MR apakah aman untuk digunakan?',
    'content'=>'Vaksin MR aman untuk digunakan karena telah mempunyai izin edar dari BPOM Vaksin MR telah mempunyai Pra-Qualifikasi WHO, selain itu telah teregistrasi dan dipasarkan di lebih dari 140 negara. Pemakaiannya pun telah lebih dari 1 Miliar dosis, termasuk di Malaysia, Iran, Kamerun, Maroko, Tunisia, Yaman, dan negara berpenduduk muslim lainnya.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'2. Apakah vaksin MR merupakan produksi dalam negeri?',
    'content'=>'Vaksin MR yang digunakan pada saat ini merupakan vaksin impor.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'3. Apakah vaksin MR proses pembuatannya menggunakan stem cells?',
    'content'=>'Vaksin MR bukan terbuat menggunakan stem cell pada saat produksinya. Bahwa beredarnya isu vaksin MR tebuat menggunakan stem cell adalah kesalahpahaman.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'4. Apakah vaksin MR proses pembuatannya menggunakan virus dari aborsi wanita sehat yang dipaparkan virus Rubella?',
    'content'=>'Vaksin MR tidak terbuat dari hasil virus aborsi wanita sehat yang dipaparkan virus rubella.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'5. Apakah vaksin MR memiliki kandungan Mercury?',
    'content'=>'Vaksin MR tidak mengandung mercury.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'6. Untuk usia berapa vaksin MR diberikan ?',
    'content'=>'Vaksin MR pada kampanye imunisasi nasional saat ini diberikan pada anak yang berusia 9 bulan sampai 15 tahun.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'7. Kenapa imunisasi MR diberikan pada anak yang berusia 9 bulan-15 tahun ?',
    'content'=>'Karena berdasarkan data surveilans kasus Rubella di Indonesia terbanyak terjadi pada usia dibawah 15 tahun, sehingga untuk mencapai target eliminasi Rubella di Indonesia maka kelompok usia dibawah 15 tahun yang menjadi fokus utama di imunisasi. Bila cakupan imunisasi MR tinggi pada kelompok dibawah 15 tahun, maka diharapkan dapat melindungi ibu hamil dan calon ibu dari penyakit Rubella. Sehingga kekebalan kelompok atau herd immunity dapat terbentuk.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'8. Bagaimana efek samping vaksin MR?',
    'content'=>'Penggunaan vaksin MR secara umum sangat aman, efek sampingnya seperti pemberian imunisasi lainnya.<ul><li>Akan timbul nyeri ringan dalam jangka wktu 24 jam setelah dilakukan vaksinasi.</li><li>Demam ringan dapat terjadi pada hari ke 7 sampai hari ke 12, efek samping ini biasanya terjadi pada 5-15% penerima vaksin. Demam ringan hanya terjadi 1 sampai 2 hari.</li><li>Ruam atau kemerahan pada kulit dapat terjadi pada hari ke 7 sampai hari ke 10 setelah dilakukan vaksinasi dan akan terjadi selama 2 hari, efek samping ini , efek samping ini biasanya terjadi pada 2% penerima vaksin.</li><li>Nyeri sendi dapat terjadi sebanyak 0-3% pada anak-anak.</li><br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'9. Apakah vaksin MR dapat menimbulkan autisme?',
    'content'=>'Hingga saat ini tidak ada fakta yang dapat menunjukan bahwa imunisasi apapun bisa menyebabkan autisme.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'10. Kemana harus melapor jika terjadi efek samping setelah imunisasi MR?',
    'content'=>'Dapat melapor ke Puskesmas terdekat atau fasilitas tempat pelaksanaan imunisasi.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'11. Pada saat kondisi bagaimana tidak di rekomendasikannya pemberian vaksin MR?',
    'content'=>'Pemberian vaksin MR ditunda apabila anak sedang demam tinggi, flu berat, diare berat atau apabila kondisi anak sedang tidak sehat dapat berkosultasi dengan petugas kesehatan.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'12. Bagaimana apabila tidak menerima vaksin MR?',
    'content'=>'Penyakit yang disebabkan virus Measles Rubella sangat bebrbahaya terlebih jika menjangkit wanita hamil karena dapat menyebabkan kecacatan CRS.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'13. Apakah ibu hamil perlu mendapat vaksin MR?',
    'content'=>'Vaksin MR tidak boleh diberikan pada wanita hamil.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'14. Apa bedanya imunisasi MR dengan MMR?',
    'content'=>'Vaksin MR untuk mencegah penyakit campak atau measles dan rubella. Sedangkan vaksin MMR berfungsi untuk mencegah penyakit gondongan atau mumps, campak atau measles, dan rubella. Kedua vaksin tersebut sangat baik namun pada penggunaannya menyesuaikan dengan suatu penyakit yang mewabah di suatu negara. Saat ini yang sedang menjadi masalah di Indonesia adalah campak dan rubella, dengan begitu kampanye imunisasi MR dilakasanakan.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'15. Jika sudah diberikan vaksin campak dan vaksin MMR, perlu diberikan lagi vaksin MR?',
    'content'=>'Vaksin MR merupakan program pemerintah yang laksanakan mulai bulan Agustus 2017, aman untuk diberikan lagi tanpa memeperhitungkan imunisasi sebelumnya.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ],
  [
    'title'=>'16. Apakah peberian vaksin MR dapat diberikan bersamaan dengan pemberian vaksin lainnya?',
    'content'=>'Vaksin MR dapat diberikan secara bersamaan dengan vaksin lainnya.<br><br>Sumber: <a href="www.infoimunisasi.com">www.infoimunisasi.com</a>'
  ]  
];
 ?>
<main class="faq pt-5">
  <img class="icon-love" src="assets/icons/love-berita.png" alt="icon love" style="margin-top: 100px;max-width: 400px;margin-right: 20px;">
  <section>
    <div class="row justify-content-center mb-3">
      <div class="col-md-8 title" data-aos="fade-up" data-aos-delay="200">
        <h2><span>FAKTA-FAKTA SEPUTAR IMUNISASI MR</span></h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
        <div class="accordion" id="accordionExample">
          <?php foreach ($faq as $key => $row): ?>
            <div class="card">
              <div class="card-header <?php echo $key == 0?'':'collapsed';?>" id="heading-<?php echo $key; ?>" data-toggle="collapse" data-target="#collapse-<?php echo $key; ?>" aria-expanded="<?php echo $key == 0 ?'true':'false'; ?>" aria-controls="collapse-<?php echo $key; ?>">
                <div class="row">
                  <div class="col-10">
                    <h2 class="mb-0">
                      <?php echo $row['title']; ?>
                    </h2>
                  </div>
                  <div class="col-2">
                      <img class="icon-faq float-right" src="assets/icons/arrow-down.png">
                  </div>
                </div>
              </div>

              <div id="collapse-<?php echo $key; ?>" class="collapse <?php echo $key == 0 ?'show':'' ?>" aria-labelledby="heading-<?php echo $key; ?>" data-parent="#accordionExample">
                <div class="card-body">
                  <?php echo $row['content']; ?>
                </div>
              </div>
            </div>            
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </section>
</main>

<?php include 'footer.php'; ?>