<?php
  function echoActiveClass($requestUri)
  {
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
      echo 'class="nav-item active"';
    else 
      echo 'class="nav-item"';
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="assets/icons/logo-01.png" type="image/x-icon" />
    <link rel="stylesheet" href="assets/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/styles/app.css?v=4.1.1" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title><?php if(isset($title)) { echo $title.' :: Ayo Imunisasi MR'; } else { echo 'Ayo Imunisasi MR'; } ?></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-pink fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="assets/icons/logo2-white.png" alt="Logo Ayo Imunisasi MR" height="30" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-0 ml-auto">
            <li <?= echoActiveClass('pengetahuan-umum') ?>>
              <a class="nav-link" href="pengetahuan-umum.php">
                Pengetahuan Umum
              </a>
            </li>
            <li <?= echoActiveClass('imunisasi-mr') ?>>
              <a class="nav-link" href="imunisasi-mr.php">
                Imunisasi MR
              </a>
            </li>
            <li <?= echoActiveClass('landasan-hukum') ?>>
              <a class="nav-link" href="landasan-hukum.php">
                Landasan Hukum
              </a>
            </li>
            <li <?= echoActiveClass('berita') ?>>
              <a class="nav-link" href="berita.php">
                Berita
              </a>
            </li>
            <li <?= echoActiveClass('faq') ?>>
              <a class="nav-link" href="faq.php">
                FAQ
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>