<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  $title = 'Home';
  include 'header.php';
?>
<style type="text/css">
  @media screen and (max-width: 767px) {
    .banner-text{
      min-height: 160px;
    }
  }
  body {
    min-height: 75rem;
  }
  .fixed-top {
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1030;
  }
</style>  
<main>
  <div id="carouselExampleIndicators" class="carousel slide banner" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="assets/images/banner-01.jpg" data-aos="fade-in" data-aos-delay="200" style="width: 100%;">
      </div>
      <div class="carousel-item">
        <img src="assets/images/banner-02.jpg" data-aos="fade-in" data-aos-delay="200" style="width: 100%;">
      </div>
      <div class="carousel-item">
        <img src="assets/images/banner-03.jpg" data-aos="fade-in" data-aos-delay="200" style="width: 100%;">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  
</main>
  <!-- end banner -->
  <!-- start berita -->
  <div class="berita-main container">
    <div class="row mb-3 berita-main-title">
      <div class="col">
        <h2 data-aos="fade-in" data-aos-delay="200">Berita</h2>
      </div>
    </div>
    <div class="row justify-content-between">
      <?php for ($i=0; $i < 3; $i++) 
      { 
        $berita = $news[$i];
        echo '<div class="col-md-3"  data-aos="zoom-in" data-aos-delay="'.(200+($i*20)).'">
                <a href="berita-detail.php?s='.$berita['slug'].'">
                  <div class="berita-main-wrapper">
                    <img src="'.$berita['image'].'" class="img-fluid mb-3" alt="'.$berita['title'].'"/>
                  </div>
                  <p>'.$berita['title'].'</p>
                </a>
              </div>';
      } ?>
    </div>
  </div>
  <!-- end berita -->

<?php include 'footer.php'; ?>
  