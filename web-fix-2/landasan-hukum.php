<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Landasan Hukum';
  include 'header.php';
?>

<main style="padding-top: 0px;">
  <div class="banner-content text-white">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="title">
            <h2 data-aoss="fade-up" data-aoss-delay="200">Landasan Hukum</h2>
          </div>
        </div>
      </div>
    </div>
    <img class="banner-love" src="assets/icons/icon-love.png" alt="icon love">
  </div>

  <div class="submenu" data-aos="fade-up" data-aos-delay="300">
    <div class="submenu-nav" id="submenu-main">
      <ul data-aos="fade-in" data-aos-delay="200">
        <li><a class="active" href="#peratuan-uu">Peaturan UU</a></li>
      <li><a href="#fatwa-mui">Fatwa MUI</a></li>
      </ul>
    </div>
  </div>
  <section data-aos="fade-up" data-aos-delay="400" class="submenu-section" id="peratuan-uu">
    <div class="row align-items-center">
      <div class="col-md-12 title title-underline">
        <h2>Tahukah Anda <span>landasan hukum</span><br>pelaksanaan <span>imunisasi</span> ?</h2>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-12">
        <div class="wrapper">
          <img src="assets/images/landasan-hukum.jpg" alt="landasan hukum" data-aos="fade-in" data-aos-delay="600">
        </div>
      </div>
    </div>

    <a href="" class="to-top">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
  <section data-aos="fade-up" data-aos-delay="400" class="submenu-section bg-pink" id="fatwa-mui">
    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="title title-line">
          <h2>Tahukah Anda penggunaan vaksin MR juga<br>diatur dalam <span>Fatwa MUI</span>?</h2>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-6">
        <p class="mt-2">
          Penggunaan vaksin MR yang merupakan produk dari Serum Institute of
          India (SII) diperbolehkan dalam
          <strong>Fatwa MUI Nomor 33 Tahun 2018</strong> setidaknya
          dikarenakan 3 hal yaitu:
        </p>
        <p>
          <ul class="pl-4">
            <li>Ada kondisi keterpaksaan (darurat syar’iyyah)</li>
            <li>Belum ditemukan vaksin MR yang halal dan suci</li>
            <li>Ada keterangan dari ahli yang kompeten dan dipercaya tentang bahaya yang ditimbulkan akibat tidak diimunisasi dan belum adanya vaksin yang halal.</li>
          </ul>
        </p>
      </div>
      <div class="col-md-6">
        <p>Salah satu yang menjadi acuan keputusan ini adalah Al-Quran surat Al-An’am ayat 119 yang berbunyi:</p>
        <p class="text-center">وَمَا لَكُمْ أَلَّا تَأْكُلُوا۟ مِمَّا ذُكِرَ ٱسْمُ ٱللَّهِ عَلَيْهِ وَقَدْ فَصَّلَ لَكُم مَّا حَرَّمَ عَلَيْكُمْ إِلَّا مَا ٱضْطُرِرْتُمْ إِلَيْهِ  وَإِنَّ كَثِيرًا لَّيُضِلُّونَ بِأَهْوَآئِهِم بِغَيْرِ عِلْمٍ ۗ إِنَّ رَبَّكَ هُوَ أَعْلَمُ بِٱلْمُعْتَدِينَ
        </p>
        <p>
          Arab-Latin: 
          <em>Wa mā lakum allā ta`kulụ mimmā żukirasmullāhi 'alaihi wa qad faṣṣala lakum mā ḥarrama 'alaikum illā maḍṭurirtum ilaīh, wa inna kaṡīral layuḍillụna bi`ahwā`ihim bigairi 'ilm, inna rabbaka huwa a'lamu bil-mu'tadīn</em>
        </p>
        <p>
          Terjemah Arti: Mengapa kamu tidak mau memakan (binatang-binatang yang halal) yang disebut nama Allah ketika menyembelihnya, padahal sesungguhnya Allah telah menjelaskan kepada kamu apa yang diharamkan-Nya atasmu, kecuali apa yang terpaksa kamu memakannya. Dan sesungguhnya kebanyakan (dari manusia) benar benar hendak menyesatkan (orang lain) dengan hawa nafsu mereka tanpa pengetahuan. Sesungguhnya Tuhanmu, Dialah yang lebih mengetahui orang-orang yang melampaui batas.
        </p>
        <p>Fatwa selengkapnya dapat Anda unduh pada tombol dibawah ini.</p>
      </div>
    </div>

    <a href="" class="to-top to-top-white">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
</main>

<?php include 'footer.php'; ?>