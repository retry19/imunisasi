<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Pengetahuan Umum';
  include 'header.php';
?>


<main style="padding-top: 0px;">
  <div class="banner-content text-white">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="title">
            <h2 data-aoss="fade-up" data-aoss-delay="200">Pengetahuan Umum</h2>
          </div>
        </div>
      </div>
    </div>
    <img class="banner-love" src="assets/icons/icon-love.png" alt="icon love">
  </div>

  <div class="submenu" data-aos="fade-up" data-aos-delay="300">
    <div class="submenu-nav" id="submenu-main">
      <ul data-aos="fade-in" data-aos-delay="200">
        <li><a class="active" href="#herd-immunity">Herd Immunity</a></li>
        <li><a href="#perbedaan-imunisasi-dan-vaksinasi">Perbedaan Imunisasi dan Vaksinasi</a></li>
        <li><a href="#gambaran-umum-imunisasi">Gambaran Umum Imunisasi</a></li>
      </ul>
    </div>
  </div>
  <section data-aos="fade-up" data-aos-delay="400" class="submenu-section" id="herd-immunity">
    <div class="row align-items-center">
      <div class="col-md-6 photo">
        <div class="wrapper">
          <img src="assets/images/herd-immunity.jpg" alt="ilustrasi herd immunity">
        </div>
        <small>Foto: Ilustrasi Herd Immunity</small><br />
        <small style="font-size: 0.6rem;">(Sumber: <a href="https://myeatandtravelstory.wordpress.com">https://myeatandtravelstory.wordpress.com</a>)</small>
      </div>
      <div class="col-md-6 title title-underline">
        <h2>Apa itu <span>Herd Immunity</span>?</h2>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-6">
        <p>
          Herd immunity dalam bahasa indonesia berarti kekebalan kelompok atau
          komunitas. Herd immunity dari suatu penyakit dapat terbentuk dengan
          cara pemberian vaksin cakupannya secara meluas atau herd immunity
          dapat tercapai melalui kekebalan alami yang terbentuk pada sebagian
          besar orang karena pernah terinfeksi penyakit tertentu lalu sembuh
          dan memiliki antibodi yang mencegahnya penyakit tersebut terinfeksi
          kembali.
        </p>
      </div>
      <div class="col-md-6">
        <p>
          Jadi cara kerja konsep herd immunity adalah jika suatu kelompok
          masyarakat memiliki kekebalan terhadap suatu virus dari penyakit
          tertentu, maka virus tersebut akan hilang dengan sendirinya. Konsep
          herd immunity merupakan konsep dimana orang-orang sehat melindungi
          orang-orang disekitarnya yang sistem imunnya kurang bagus atau yang
          tidak bisa menerima vaksin, sehingga orang-orang yang imunnya kurang
          bagus atau yang tidak bisa menerima vaksin tidak mudah tertular
          penyakit tertentu.
        </p>
      </div>
    </div>

    <a href="" class="to-top">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
  <section data-aos="fade-up" data-aos-delay="400" class="submenu-section bg-pink" id="perbedaan-imunisasi-dan-vaksinasi">
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="title title-line">
          <h2>Tahukan Anda bedanya <span>imunisasi dan vaksinasi</span>?</h2>
        </div>
      </div>
      <div class="col-md-6 photo">
        <div class="wrapper">
          <img src="assets/images/vaksin.jpg" alt="ilustrasi perbedaan vaksin dan imunisasi">
        </div>
        <small>Foto: Ilustrasi perbedaan vaksin dan imunisasi</small><br />
        <small style="font-size: 0.6rem;">(Sumber: <a href="https://thejakartapost.com">https://thejakartapost.com</a>)</small>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-6">
        <p>
          Berdasarkan Pusat Pengendalian dan Pencegahan Penyakit Amerika
          Serikat atau CDC, vaksinasi adalah suatu kegiatan memasukkan vaksin
          kedalam tubuh seseorang dengan tujuan untuk menghasilkan kekebalan
          terhadap suatu penyakit tertentu. Sedangkan imunisasi adalah suatu
          proses ketika seseorang menjadi kebal terhadap penyakit tertentu yg
          dilakukan melalui vaksinasi.
        </p>
      </div>
      <div class="col-md-6">
        <p>
          Proses imunisasi dapat dianalogikan sebagai berikut, apabila
          seseorang terkena penyakit maka sistem kekebalan tubuh akan berjuang
          untuk melawan penyakit tersebut. Serupa dengan proses imunisasi,
          setelah dilakukan vaksinasi maka vaksin akan merangsang sistem
          kekebalan tubuh agar kebal dalam menghadapi penyakit tertentu
          dikemudian hari.
        </p>
      </div>
    </div>

    <a href="" class="to-top to-top-white">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
  <section data-aos="fade-up" data-aos-delay="400" class="submenu-section" id="gambaran-umum-imunisasi">
    <div class="row align-items-center">
      <div class="col-md-6 photo">
        <div class="wrapper">
          <img src="assets/images/imunisasi.jpg" alt="ilustrasi imunisasi">
        </div>
        <small>Foto: Ilustrasi imunisasi</small><br />
        <small style="font-size: 0.6rem;">(Sumber: <a href="https://aktual.com">https://aktual.com</a>)</small>
      </div>
      <div class="col-md-6 title title-underline">
        <h2><span>Imunisasi</span> itu apa sih?</h2>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-6">
        <p>
          Imunisasi adalah suatu tindakan preventif dalam mencegah terjadinya penularan Penyakit yang Dapat Dicegah Dengan Imunisasi atau PD3I. Dengan cara menyutikan vaksin ke dalam tubuh seseorang. Sehingga memungkinkan orang tersebut kebal atau resisten dengan penyakit tertentu. Berdasarkan definisi tersebut, apabila ada anak yang diimunisasi, maka anak tersebut diberikan kekebalan terhadap PD3I. Imunisasi bertujuan untuk menekan angka kesakitan, kecacatan, serta kematian yang diakibatkan oleh penyakit yang dapat dicegah melalui imunisasi. Penyakit yang dapat dicegah melalui imunisasi diantaranya adalah:
        </p>
        <p>
          <ul class="pl-4">
            <li>Difteri</li>
            <li>Pertusis</li>
            <li>Tetanus</li>
            <li>Tuberculosis (TBC)</li>
            <li>Campak</li>
            <li>Poliomielitis</li>
            <li>Hepatitis B</li>
            <li>Hemofilus Influenza tipe b (Hib)</li>
            <li>HPV (Human papiloma virus)</li>
            <li>Hepatitis A</li>
          </ul>
        </p>
      </div>
      <div class="col-md-6" >
        <p>
          Sedangkan vaksin merupakan suatu zat yang dimasukan kedalam tubuh pada saat imunisasi yang berupa antigen yang terdiri dari mikroorganisme yang sudah mati, masih hidup namun dilemahkan, masih utuh atau sebagiannya. Mikroorganisme yang sudah diolah atau toksin mikroorganisme yang sudah diolah lalu menjadi toksoid atau protein rekombinan. Lalu mikroorganisme tersebut diberikan kepada seseorang sehingga seseorang dapat memiliki kekebalan yang spesifik terhadap penyakit tertentu. Berdasarkan definisi tersebut vaksin merupakan antigen yang berupa mikroorganisme yang diolah sedemikian rupa, bermanfaat untuk merangsang kekebalan pada tubuh seseorang terhadap penyakit tertentu. Dibawah ini merupakan program imunisasi dalam Permenkes No. 12 Tahun 2017.
        </p>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-12 photo photo-center">
        <div class="wrapper">
          <img src="assets/images/program-imunisasi.jpg" alt="program imunisasi" class="img-fluid">
        </div>
      </div>
    </div>
    <br>
    <a href="" class="to-top">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
</main>

<?php include 'footer.php'; ?>