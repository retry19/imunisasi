// smooth scroll when click anchor
$('a[href*=\\#]').on('click', function(event){     
  event.preventDefault();
  $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
});

// add bg-color on navbar when scroll
$(function () {
  $(document).scroll(function () {
	  var $nav = $(".fixed-top");
	  $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
	});
});

$('.to-top').click(function() {
  $("html, body").animate({
    scrollTop: 0
  }, 600);
  return false;
});


// for active class when scroll
$(window).scroll(function() {
  var scrollDistance = $(window).scrollTop();

  $('.submenu-section').each(function(i) {
      if ($(this).position().top <= scrollDistance + 200) {
          $('.submenu-nav ul a.active').removeClass('active');
          $('.submenu-nav ul a').eq(i).addClass('active');
      }
  });
}).scroll();