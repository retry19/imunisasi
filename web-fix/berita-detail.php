<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  
  $index = find($_GET['s'],$news);
  $berita = $news[$index];

  $title = $berita['title'];
  $navbar = 'pink';
  include 'header.php';
?>

<div class="detail-berita container">
  <div class="row justify-content-center">
    <div class="col-md-8" data-aos="fade-up" data-aos-delay="200">
      <div class="wrapper">
        <img src="<?php echo $berita['image']; ?>" alt="<?php echo $berita['title']; ?>" />
      </div>
      <small>(Sumber: <a href="<?php echo getHostUrl($berita['link_source']); ?>"><?php echo getHostUrl($berita['link_source']); ?></a>)</small>
      <small
        ><?php echo $berita['date']; ?><br /><?php echo $berita['author']; ?></small
      >
      <h4><?php echo $berita['title']; ?></h4>
      <label><?php echo $berita['reference']; ?></label>
      <?php echo $berita['content']; ?>
    </div>
  </div>
</div>

<?php include 'footer.php'; ?>