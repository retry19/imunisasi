<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  
  $title = 'Berita';
  $navbar = 'pink';
  include 'header.php';
?>

<div class="berita">
  <img class="icon-love" src="assets/icons/icon-love.png" alt="icon love">
  <div class="row justify-content-center">
    <?php 
      foreach ($news as $i => $row) {
        if (($i+2)%2==0) {
          echo '</div><div class="row justify-content-center">';
        }
        echo '<div class="col-md-5 mb-4" data-aos="fade-in" data-aos-delay="'.($i+2).'00">
                <a href="berita-detail.php?s='.$row['slug'].'">
                  <div class="wrapper mb-3">
                    <img src="'.$row['image'].'" alt="'.$row['title'].'" />
                  </div>
                  <p>'.$row['title'].'</p>
                  <label class="mt-1">'.$row['reference'].'</label>
                </a>
              </div>';
      }
    ?> 
  </div>
</div>

<?php include 'footer.php'; ?>