<?php define('imunisasi', 'imunisasimr2020');

  $title = 'Imunisasi MR';
  include 'header.php';
?>

<div class="banner-content text-white">
  <div class="row title">
    <div class="col-md-12">
      <h2 data-aos="fade-up" data-aos-delay="200">Imunisasi MR</h2>
    </div>
  </div>
  <img class="banner-love" src="assets/icons/icon-love.png" alt="icon love">
</div>
  
<div class="submenu">
  <div class="submenu-nav" id="submenu-main">
    <ul  data-aos="fade-in" data-aos-delay="400">
      <li><a class="active" href="#campak-dan-rubella">Campak dan Rubella</a></li>
      <li><a href="#epidemiologi">Epidemiologi</a></li>
      <li><a href="#vaksin-mr">Vaksin MR</a></li>
    </ul>
  </div>
</div>

<main>
  <section class="submenu-section" id="campak-dan-rubella">
    <div class="row">
      <div class="col-md-6">
        <div class="title title-fill">
          <h2 class="py-3 pr-3" data-aos="fade-in" data-aos-delay="600">Campak dan Rubella<br>itu apa?</h2>
        </div>
        <div class="desc mt-4" data-aos="fade-right" data-aos-delay="600">
          <p>
            Campak merupakan penyakit yang dapat disebabkan oleh virus yang
            dapat ditularkan melalui bersin dan batuk penderita. Campak termasuk
            penyakit yang mudah menular. Gejalanya berupa demam yang tinggi dan
            bercak kemerahan pada kulit atau rash juga biasanya disertai batuk
            pilek. Gejalanya dapat berbahaya jika disertai dengan penyakit lain
            seperti Pneumonia, Diare, dan Meningitis karena bisa berakhir pada
            kematian. Ketika penderita Campak berinteraksi erat dengan orang
            yang bukan penderita memiliki potensi 90% untuk tertular. Namun hal
            tersebut dapat diatasi dengan imunisasi agar tidak terserang virus
            Campak.
          </p>
          <p>
            Sedangkan penyakit Rubella merupakan penyakit ringan, kelompok usia
            yang rentan mengalami infeksi Rubella adalah anak dan dewasa muda.
            Namun infeksi Rubella yang harus diwaspadai dalam kesehatan
            masyarakat adalah infeksi yang menjangkit wanita hamil. Infeksi
            Rubella yang menyerang wanita hamil yang pada umumnya menjangkit
            saat trimester pertama. Infeksi Rubella pada wanita hamil dapat
            berdampak kepada janin atau bayi yang dilahirkan yang menyebabkan
            keguguran atau kecacatan yang disebabkan oleh Sindrom Rubella
            Kongenital atau biasa disebut CRS.
          </p>
          <p>
            Penyakit ini memiliki potensi menjadi wabah apabila imunisasi Campak
            dan Rubella cakupannya rendah dan tidak terbentuknya kekebalan
            kelompok atau herd immunity.
          </p>
        </div>
      </div>
      <div class="col-md-6 photo" data-aos="fade-left" data-aos-delay="600">
        <div class="wrapper">
          <img src="assets/images/campak-dan-rubella.jpg" alt="ilustrasi campak dan rubella">
        </div>
        <small>Foto: Ilustrasi Campak dan Rubella</small><br />
        <small style="font-size: 0.6rem;">(Sumber: <a href="https://www.honestdocs.id">https://www.honestdocs.id</a>)</small>
      </div>
    </div>
    <a href="" class="to-top">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
  <section id="epidemiologi" class="submenu-section bg-pink">
    <div class="row" data-aos="fade-down" data-aos-delay="800">
      <div class="col-md-12 title" style="border-bottom: 1px solid #fff;">
        <h2>Lalu bagaimana penularan<br>Campak dan Rubella?</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 text-left desc" data-aos="fade-right" data-aos-delay="800">
        <p>
          Campak yang biasa disebut dengan morbili atau measles adalah penyakit yang disebabkan oleh virus serta mudah menular atau infeksius. Sebelum adanya imunisasi Campak pada tahun 1980 diperkiran lebih dari 20 juta orang di dunia terdampak penyakit Campak dan 2,6 juta orang mengalami kematian setiap tahunnya. Kelompok usia yang mendominasi terkena Campak didominasi oleh anak yang berusia dibawah 5 tahun. Namun saat telah dimulai vaksinasi melalui program imunisasi pada tahun 2000 yang dilakukan di negara-negara yang berisko tinggi termasuk Indonesia sebagai negara yang memiliki kasus penularan terbesar hingga tahun 2012 angka kematian yang diakibatkan oleh Campak secara gobal mengalami penuranan hingga 78%.
        </p>
        <p>
          Sedangkan Rubella adalah penyakit yang disebabkan oleh togavirus yang berjenis rubivirus yang termasuk pada golongan virus RNA. Virus Rubella merupakan virus yang mudah mati jika terkena sinar ultra violet, bahan kimia, bahan asam, dan pemanasan. Rubella dapat menular lewat saluran pernapasan yang disebabkan oleh bersin dan batuk penderita. Virus Rubella berkembang biak di bagian nasofaring dan kelenjar getah bening serta menginfeksi tubuh dalam rentang waktu 4 sampai 7 hari terhitung sejak virus masuk kedalam tubuh. Namun masa inkubasinya 14 sampai 21 hari. Gejalanya  dapat berupa: 
        </p>
        <p>
          <ol type="a" class="pl-4">
            <li>Demam ringan dengan suhu tubuh 37,2 derajat celcius serta bercak merah dengan dibarengi gejala lain seperti pembesaran kelenjar limfe di sub occipital, belakang telinga, dan leher belakang.</li>
            <li>Gejala yang terjadi jika menjangkit kepada anak dapat berupa gejala demam secara ringan atau tidak terjadi gejala sama sekali.</li>
          </ol>
      </div>
      <div class="col-md-6 text-left desc" data-aos="fade-left" data-aos-delay="800">
          <div class="photo mb-3">
            <div class="wrapper">
              <img src="assets/images/epidemiologi.jpg" alt="ilustrasi epidemiologi" class="img-fluid">
            </div>
            <small class="text-white">Foto: Ilustrasi Epidemiologi</small><br />
            <small style="font-size: 0.6rem; color: #fff;">(Sumber: <a class="text-white" href="https://tirto.id">https://tirto.id</a>)</small>
          </div>
          <ol type="a" start="3">
            <li>
              Sedangkan gejala pada wanita hamil dapat menimbulkan peradangan serta kekakuan pada sendi yang biasa disebut arthritis atau arthralgia. Penyakit Rubella yang menjangkit pada ibu hamil dan janin yang terjadi saat kehamilan trimester pertama bisa berdampak keguguran pada kandungan atau bayi lahir dengan kelainan Congenital Rubella Syndrome atau CRS. Kelainan CRS bisa berupa: 
              <ul class="pl-4">
                <li>Kelainan pada jantung <br>Bisa berupa stenosis katup pulmonal, defek septum atrial, defek septum ventrikel, dan defek septum atrial.</li>
                <li>Kelainan pada mata <br>Bisa berupa katarak kogenital, galukoma kogenital, dan pigmentary retinopati.</li>
                <li>Kelainan pada pendengaran</li>
                <li>Kelainan pada sistem saraf pusat <br>Bisa berupa retardasi mental, mikrocephalia, meningoesefalitis.</li>
                <li>Serata kelainan lainnya <br>Bisa berupa radioluscent bone, puroura, splenomegali, dan ikterik yang muncul dalam 24 jam setelah lahir.</li>
              </ul>
            </li>
          </ol>
        </p>
      </div>
    </div>
    <a href="" class="to-top to-top-white">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
  <section class="submenu-section" id="vaksin-mr">
    <div class="row">
      <div class="col-md-6"  data-aos="fade-right" data-aos-delay="800">
        <div class="title mb-4">
          <h2>Apa dan bagaimana<br>manfaat dari <span>vaksin MR</span> ?</h2>
        </div>
        <div class="photo">
          <div class="wrapper">
            <img src="assets/images/vaksin-mr.jpg" alt="ilustrasi vaksin campak dan rubella">
          </div>
          <small>Foto: Ilustrasi Vaksin Campak dan Rubella</small><br />
          <small style="font-size: 0.6rem;">(Sumber: <a href="https://mediaindonesia.com">https://mediaindonesia.com</a>)</small>
        </div>
      </div>
      <div class="col-md-6 desc" data-aos="fade-left" data-aos-delay="800">
        <p>
          Vaksin Measles Rubella atau vaksin MR merupakan klasifikasi vaksin hidup yang dilemahkan atau live attenuated yang berbentuk serbuk kering yang berwarna putih serta kekuningan untuk menggunakannya dibutuhkan pelarut serta pengencer yang disediakan oleh produsen vaksin yang sama. Kemasan vaksin terdiri dari 10 dosis per vial. Setiap dosis vaksin MR tersebut mengandung:
        </p>
        <p>
          <ul class="pl-4">
            <li>Virus Campak sejumlah 1000 CCID50</li>
            <li>Virus Rubella sejumlah 1000 CCID50</li>
          </ul>
        </p>
        <p>
          Imunisasi campak dan rubella dapat memberikan manfaat untuk melindungi anak dari kecacatan dan kematian yang diakibatkan oleh pneumonia, diare, kerusakan otak, ketulian, kebutaan dan penyakit jantung bawaan.
        </p>
        <p>
          Vaksin MR juga sangat aman digunakan dan berkualitas karena telah mempun yai ijin edar dari Badan Pengawas Obat dan Makanan atau BPOM. Selain itu mendapatkan pra qualifikasi dari badan kesehatan dunia yaitu World Health Organization atau WHO dan telah digunakan sejak tahun 1989 lebih dari 140 negara termasuk negara berpenduduk Muslim.
        </p>
      </div>
    </div>
    <a href="" class="to-top">
      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="15" cy="15" r="14.5" stroke="white"/>
        <path d="M9 18L15 11L21 18" stroke="white" stroke-width="2" stroke-linecap="round"/>
      </svg>
    </a>
  </section>
</main>

<?php include 'footer.php'; ?>