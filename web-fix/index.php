<?php define('imunisasi', 'imunisasimr2020');

  include 'ref-berita.php';
  $title = 'Home';
  include 'header.php';
?>
<style type="text/css">
  @media screen and (max-width: 767px) {
    .banner-text{
      min-height: 160px;
    }
  }
</style>  
  <div id="carouselExampleIndicators" class="carousel slide banner" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="row text-white">
          <div class="col-lg-8 banner-text" data-aos="fade-up" data-aos-delay="200">
            <h1>Imunisasi MR penting karena kesehatan anak merupakan investasi terbesar.</h1>
          </div>
          <div class="col-lg-4 banner-image" data-aos="zoom-in" data-aos-delay="200">
            <img src="assets/images/main-photo.png" class="img-fluid" />
            <small>(sumber: dokumentasi pribadi)</small>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <div class="row text-white">
          <div class="col-lg-7 order-sm-2 banner-text" data-aos="fade-up" data-aos-delay="200">
            <h1>KESEHATAN ANAK ADALAH SEGALANYA. LINDUNGI ANAK DARI BAHAYA CAMPAK & RUBELLA.</h1>
          </div>
          <div class="col-lg-4 order-sm-1 banner-image" data-aos="zoom-in" data-aos-delay="200">
            <img src="assets/images/second-photo.png?v=4" class="img-fluid" />
            <small>(sumber: dokumentasi pribadi)</small>
          </div>
          
        </div>
      </div>
      <div class="carousel-item">
        <div class="row text-white">
          <div class="col-lg-8 banner-text" data-aos="fade-up" data-aos-delay="200">
            <h1>IMUNISASI MERUPAKAN SEBUAH BENTUK KASIH SAYANG KITA SEBAGAI ORANG TUA.</h1>
          </div>
          <div class="col-lg-4 banner-image" data-aos="zoom-in" data-aos-delay="200">
            <img src="assets/images/third-photo.png?v=4" alt="Foto anak-anak sedang bermain" class="img-fluid" />
            <small>(sumber: dokumentasi pribadi)</small>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!-- end banner -->
  <!-- start berita -->
  <div class="berita-main container">
    <div class="row mb-3 berita-main-title">
      <div class="col">
        <h2>Berita</h2>
      </div>
    </div>
    <div class="row justify-content-between">
      <?php for ($i=0; $i < 3; $i++) 
      { 
        $berita = $news[$i];
        echo '<div class="col-md-3"  data-aos="zoom-in" data-aos-delay="'.($i+2).'00">
                <a href="berita-detail.php?s='.$berita['slug'].'">
                  <div class="berita-main-wrapper">
                    <img src="'.$berita['image'].'" class="img-fluid mb-3" alt="'.$berita['title'].'"/>
                  </div>
                  <p>'.$berita['title'].'</p>
                </a>
              </div>';
      } ?>
    </div>
  </div>
  <!-- end berita -->

<?php include 'footer.php'; ?>
  